
//Hecho por Federico Capdeville
//Probado con el Blue Pill (STM32F103C8T6)

#define LED PC13            //El led del bluepill

//Tengo 4 variables que envio desde la compu, que hacen cosas distintas
/*
 * q ==> prende el led del blue pill
 * w ==> apaga el led del blue pill
 * e ==> envia un string a la compu
 * r ==> tooglea el led del blue pill
 */


int a = 0;

void setup() 
{
  pinMode(LED, OUTPUT);      //Coloco un led en la pata PB12

  Serial.begin(9600);  
}


void loop() 
{
  while (Serial.available()) 
  {
    uint8 input = Serial.read();

    switch(input) 
    {
      case 'q':
        digitalWrite(LED, LOW);    //Prende el led (no entiendo porque pero con LOW lo prende)
        a = 0;
        break;
  
      case 'w':
        digitalWrite(LED, HIGH);   //Apaga el led (no entiendo porque pero con HIGH lo apaga)
        a = 1;
        break;
        
      case 'e':
          Serial.print("\n");
          Serial.println("Escribiste la e.");
          break;
  
      case 'r':                   //Toglea el led que esta en el bluepill
        if (a == 0)
        {
          a = 1;
          digitalWrite(LED, HIGH);
        }
        else
        {
          a = 0;
          digitalWrite(LED, LOW);
        }
        break;
    }
  }  
}
